all:	vulnerable vulnerable2 autobuf

vulnerable: vulnerable.cpp
	g++ -m32 -fno-stack-protector -z execstack -o vulnerable vulnerable.cpp

vulnerable2: vulnerable2.cpp
	g++ -m32 -fno-stack-protector -z execstack -o vulnerable2 vulnerable2.cpp

autobuf: autobuf.cpp
	g++ -m32 -o autobuf autobuf.cpp

clean:
	rm -f *.o vulnerable vulnerable2 autobuf
