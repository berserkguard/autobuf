#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Must supply an argument!\n");
		return 1;
	}

	char buf[20];

	printf("Checking the password...\n");
	strcpy(buf, argv[1]);
	return 0;
}

