# Autobuf - Buffer Overflow Exploit Tool #

### Overview ###

Autobuf is a simple tool written in C++ for Linux that aids in finding buffer overflow exploits. It calls gdb on a target program, progressively trying larger inputs and checking the register values until the current instruction pointer is overwritten. It then outputs the offset (in bytes) of this address from the beginning of the buffer. Once this offset is known, it becomes almost trivial to grab shellcode from the net and get it to execute inside the target program. It also calls readelf to output some useful information, such as the executable's entry point address and whether the stack is executable or not.

### Compile ###

To compile the program, you must have Linux with g++ installed and 32-bit compilation capabilities. Just run "make" from a terminal to build Autobuf and example programs using the given Makefile.

### Usage ###

./autobuf *program_name*

### Examples ###

Two simple example programs to exploit: vulnerable and vulnerable2. The first example takes no command line arguments, but uses gets() to (unsafely) read input from the user. The second example takes a single command line argument, which is copied (unsafely) via strcpy() into a buffer. You can see Autobuf in action by running "./autobuf vulnerable" or "./autobuf vulnerable2" from the terminal.

### Output ###

```
#!

berserkguard@ubuntu-VM:~/Documents/norby2/autobuf$ ./autobuf vulnerable2
Reading executable info...
Start address: 0x8048350
Stack info: Read Write Execute 

Finding offset to $eip...
The program has no registers now.
Trying input: AAAAAAAAAAAAAAAAAAAAABCD
The program has no registers now.
Trying input: AAAAAAAAAAAAAAAAAAAAAABCD
The program has no registers now.
Trying input: AAAAAAAAAAAAAAAAAAAAAAABCD
The program has no registers now.
Trying input: AAAAAAAAAAAAAAAAAAAAAAAABCD
The program has no registers now.
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAABCD
The program has no registers now.
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAAABCD
The program has no registers now.
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAAAABCD
The program has no registers now.
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAAAAABCD
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAAAAAABCD
Found addr: 0xf7e2da00
Offset: 28
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAABCD
Found addr: 0xf7e20044
Offset: 29
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABCD
Found addr: 0xf7004443
Offset: 30
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABCD
Found addr: 0x444342	0
Offset: 31
Trying input: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABCD
Found addr: 0x44434241
Offset: 32
Exact match! $eip overwritten!
```


### Planned Updates ###

* Allow user to specify shellcode and automatically build out payload
* More sophisticated exploit finding (second input, third input, etc.)
* Use readelf to display basic executable info & defenses.